unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, IdBaseComponent, IdComponent, IdTCPConnection, superobject,
  IdTCPClient, IdHTTP, IdURI, IdMultipartFormData, HTTPApp, EncdDecd;

type
  TForm1 = class(TForm)
    IdHTTP1: TIdHTTP;
    Memo1: TMemo;
    Button1: TButton;
    Button2: TButton;
    Memo2: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure IdHTTP1Connected(Sender: TObject);
    procedure IdHTTP1Disconnected(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

const
  UnixStartDate: TDateTime = 25569.0; // 01/01/1970

var
  Form1: TForm1;

implementation

{$R *.dfm}

function DateTimeToUnix(dtDate: TDateTime): Longint;
begin
  Result := Round((dtDate - UnixStartDate) * 86400);
end;

function UnixToDateTime(USec: Longint): TDateTime;
begin
  Result := (Usec / 86400) + UnixStartDate;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  get_url: String;
  login: String;
  password: String;

  resp: TMemoryStream;
  ansi_string : String;
  wide_string: WideString;

begin
  get_url := 'http://my.host';
  login := 'trainer';
  password := 'trainer';
  resp := TMemoryStream.Create;
  try
   IdHTTP1.Port := 80;
   IdHTTP1.Request.Connection := 'keep-alive';
   IdHTTP1.Request.Accept := 'application/json';
   IdHTTP1.Request.CustomHeaders.FoldLines := False;
   IdHTTP1.Request.CustomHeaders.Add('Authorization: Basic ' + EncodeString(login + ':' + password));

   ansi_string := IdHTTP1.Get(get_url);
   ansi_string := UTF8Decode(ansi_string);
   Memo1.Lines.Add(ansi_string);

  finally
   resp.Free
  end;
end;

procedure TForm1.IdHTTP1Connected(Sender: TObject);
begin
 Memo1.Lines.Add('Connect');
end;

procedure TForm1.IdHTTP1Disconnected(Sender: TObject);
begin
 Memo1.Lines.Add('Disconnect');
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  post_url: String;
  login: String;
  password: String;

  params: TIdMultiPartFormDataStream;
  resp: String;

  response_structure, user_structure, training_structure: ISuperObject;
  json_filepath: String;

begin
  post_url := 'http://my.host';
  login := 'trainer';
  password := 'trainer';
  json_filepath := '.\data.json';

  // Build JSON Structure
  training_structure := SO;
  training_structure.I['training_id'] := 127;
  training_structure.S['training_title'] := '�������� ����������';
  training_structure.I['training_date'] := 1567969611;
  training_structure.S['user_role'] := '��������';
  training_structure.I['user_mark'] := 5;
  training_structure.S['comment'] := '����� ����� ���� ��� �����������';

  user_structure := SO;
  user_structure.I['user_id'] := 12;
  user_structure.O['trainings'] := SA([]);
  user_structure.A['trainings'].O[0] := training_structure;

  response_structure := SO;
  response_structure.O['users'] := SA([]);
  response_structure.A['users'].O[0] := user_structure;

  response_structure.SaveTo(json_filepath, False, True);
  Memo2.Lines.LoadFromFile(json_filepath);

  try
   IdHTTP1.Port := 80;
   IdHTTP1.Request.Connection := 'keep-alive';
   IdHTTP1.Request.ContentType := 'multipart/form-data';
   IdHTTP1.Request.CustomHeaders.FoldLines := False;
   IdHTTP1.Request.CustomHeaders.Add('Authorization: Basic ' + EncodeString(login + ':' + password));

   params := TIdMultiPartFormDataStream.Create;
   params.AddFile('file', json_filepath, 'text/json');

   resp := IdHTTP1.Post(post_url, params);
   Memo2.Lines.Add(resp);

  finally
   params.Free
  end;

end;

end.
